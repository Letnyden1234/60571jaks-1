<?php namespace App\Models;
use CodeIgniter\Model;
class RatingModel extends Model
{
    protected $table = 'apartament'; //таблица, связанная с моделью
    public function getRating($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}