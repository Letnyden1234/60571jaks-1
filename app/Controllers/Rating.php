<?php namespace App\Controllers;

use App\Models\RatingModel;

class Rating extends BaseController


{
    public function index() //Обображение всех записей
    {
        $model = new RatingModel();
        $data ['rating'] = $model->getRating();
        echo view('rating/view_all', $data);
    }

    public function view($id = null) //отображение одной записи
    {
        $model = new RatingModel();
        $data ['rating'] = $model->getRating($id);
        echo view('rating/view', $data);
    }
}