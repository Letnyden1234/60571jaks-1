<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <h2>Список квартиры</h2>

        <?php if (!empty($rating) && is_array($rating)) : ?>

            <?php foreach ($rating as $item): ?>

                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row">
                        <div class="col-md-4 d-flex align-items-center">
                            <?php if ($item['adress'] == 0) : ?>
                                <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/2829/2829841.svg" class="card-img" alt="<?= esc($item['adress']); ?>">
                            <?php else:?>
                                <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/163/163801.svg" class="card-img" alt="<?= esc($item['adress']); ?>">
                            <?php endif ?>
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title"><?= esc($item['square']); ?></h5>
                                <p class="card-text"><?= esc($item['price']); ?></p>
                                <a href="<?= base_url()?>/index.php/rating/view/<?= esc($item['id']); ?>" class="btn btn-primary">Просмотреть</a>
                                <p class="card-text"><small class="text-muted">199</small></p>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>
        <?php else : ?>
            <p>Невозможно найти квартиру.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?><?php
