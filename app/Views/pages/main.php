<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="jumbotron text-center">
        <h1 class="display-4">ЭТАЖИ</h1>
        <p class="lead">Это приложение поможет вам продать/купить/арендовать недвижимость.</p>
        <a class="btn btn-primary btn-lg" href="auth/login" role="button">Войти</a>
    </div>
<?= $this->endSection() ?>
