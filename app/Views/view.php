<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php use CodeIgniter\I18n\Time; ?>
        <?php if (!empty($rating)) : ?>
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center">
                        <?php if ($rating['gender'] == 0) : ?>
                            <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/2829/2829841.svg" class="card-img" alt="<?= esc($rating['name']); ?>">
                        <?php else:?>
                            <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/163/163801.svg" class="card-img" alt="<?= esc($rating['name']); ?>">
                        <?php endif ?>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= esc($rating['name']); ?></h5>
                            <p class="card-text"><?= esc($rating['description']); ?></p>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Пол:</div>
                                <?php if ($rating['gender'] == 0) : ?>
                                    <div class="text-muted">Женский</div>
                                <?php else:?>
                                    <div class="text-muted">Мужской</div>
                                <?php endif ?>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Дата рождения:</div>
                                <div class="text-muted"><?= esc($rating['birthday']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Рейтинг создан:</div>
                                <div class="text-muted"><?= esc(Time::parse($rating['created_at'])->toDateString() ); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Текущий рейтинг:</div>
                                <span class="badge badge-info">199</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php else : ?>
            <p>Рейтинг не найден.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>