-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Мар 29 2021 г., 02:46
-- Версия сервера: 8.0.23-0ubuntu0.20.04.1
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60571jaks`
--

-- --------------------------------------------------------

--
-- Структура таблицы `apartament`
--

CREATE TABLE `apartament` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `adress` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'адрес',
  `floor` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'этаж',
  `num_apart` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'номер_кв',
  `rooms` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'колво комнат',
  `entrance` varchar(20) NOT NULL COMMENT 'подъезд',
  `square` int NOT NULL COMMENT 'площадь',
  `price` varchar(20) NOT NULL COMMENT 'цена',
  `typeofdeal` varchar(20) NOT NULL COMMENT 'тип сделки',
  `typeofprop` varchar(20) NOT NULL COMMENT 'тип недвижимости'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `apartament`
--

INSERT INTO `apartament` (`id`, `user_id`, `adress`, `floor`, `num_apart`, `rooms`, `entrance`, `square`, `price`, `typeofdeal`, `typeofprop`) VALUES
(1, 1, 'Сургут, Мелик-Карамова 92', '3', '40', '3', '4', 60, '40000', 'аренда', 'вторичный'),
(2, 2, 'Сургут, Мелик-Карамова 4', '18', '1054', '2', '10', 50, '3200000', 'продажа', 'первичный'),
(3, 3, 'Сургут, Ленина 4', '5', '18', '2', '3', 46, '2500000', 'покупка', 'вторичный'),
(4, 4, 'Сургут, Пролетпрский 35', '7', '41', '3', '4', 56, '3700000', 'продажа', 'первичный'),
(5, 5, 'Сургут, 30 лет победы 45', '9', '367', '1', '6', 32, '1700000', 'продажа', 'вторичный'),
(6, 6, 'Лянтор, Ленина 5', '2', '4', '2', '1', 58, '26200', 'аренда', 'первичный'),
(7, 7, 'Сургут, Джержинского 18', '2', '102', '3', '3', 91, '3650000', 'продажа', 'вторичный'),
(8, 5, 'Сургут, Каралинского 14', '12', '112', '4', '2', 82, '6200000', 'покупка', 'первичный'),
(9, 4, 'пос. Белый Яр, Есенина 12', '3', '5', '1', '1', 35, '2900000', 'продажа', 'первичный'),
(10, 2, 'пос. Солнечный, Космонавтов 12', '8', '135', '4', '8', 96, '32000', 'аренда', 'вторичный '),
(11, 3, 'Сургут, Университетская 31', '6', '78', '2', '5', 120, '8100000', 'покупка', 'первичный'),
(12, 6, 'Сургут, Комсомольский 38', '9', '60', '3', '4', 70, '25000', 'аренда', 'вторичный'),
(13, 1, 'Сургут, Крылова 36', '1', '4', '2', '1', 44, '2100000', 'продажа', 'вторичный'),
(14, 3, 'Сургут, Усольцева 13', '11', '134', '3', '2', 68, '29000', 'аренда', 'первичный'),
(15, 4, 'Сургут, Быстринская 12', '8', '12', '3', '4', 73, '4560000', 'продажа', 'вторичный');

-- --------------------------------------------------------

--
-- Структура таблицы `commercialprop`
--

CREATE TABLE `commercialprop` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `adress` varchar(50) NOT NULL COMMENT 'адрес',
  `entrance` varchar(20) NOT NULL COMMENT 'подъезд',
  `floor` varchar(20) NOT NULL COMMENT 'этаж',
  `num_prop` int NOT NULL,
  `rooms` varchar(20) NOT NULL COMMENT 'колво комнат',
  `square` varchar(20) NOT NULL COMMENT 'площадь',
  `price` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'цена',
  `typeofdeal` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'тип сделки',
  `typeofprop` varchar(20) NOT NULL COMMENT 'тип недвижимости'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `commercialprop`
--

INSERT INTO `commercialprop` (`id`, `user_id`, `adress`, `entrance`, `floor`, `num_prop`, `rooms`, `square`, `price`, `typeofdeal`, `typeofprop`) VALUES
(1, 1, 'Сургут, югорская 1', '1', '1', 2, '6', '650', '5620000', 'аренда', 'вторичный'),
(2, 2, 'ленина 1', '1', '2', 20, '4', '450', '45630000', 'продажа', 'первичный'),
(3, 3, 'Сургут, промышленная 3', '1', '3', 1, '305', '890', '60000000', 'покупка', 'первичный'),
(4, 4, 'Сургут,  Университетская 9', '2', '2', 45, '2', '200', '15000000', 'продажа', 'вторичный'),
(5, 5, 'пос. Белый Яр, Горького 3', '1', '1', 5, '3', '300', '150000', 'аренда', 'вторичный'),
(6, 6, 'Сургут, Ленина 24', '2', '2', 4, '4', '450', '650000', 'аренда', 'первичный'),
(7, 7, 'Сургут, Киртбая 20', '3', '1', 2, '4', '260', '98000', 'аренда', 'вторичный'),
(8, 5, 'Лянтор, Есенина 20', '3', '2', 1, '2', '100', '65000', 'аренда', 'вторичный');

-- --------------------------------------------------------

--
-- Структура таблицы `garage`
--

CREATE TABLE `garage` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `adress` varchar(50) NOT NULL COMMENT 'адрес',
  `square` varchar(20) NOT NULL COMMENT 'площадь',
  `typeofdeal` varchar(20) NOT NULL COMMENT 'тип сделки',
  `typeofprop` varchar(20) NOT NULL COMMENT 'тип недвижимости',
  `price` int NOT NULL COMMENT 'цена'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `garage`
--

INSERT INTO `garage` (`id`, `user_id`, `adress`, `square`, `typeofdeal`, `typeofprop`, `price`) VALUES
(1, 1, 'Сургут, Гагарина 10', '50', 'аренда', 'вторичный', 25000),
(2, 2, 'Сургут, Республики 12', '35', 'аренда', 'вторичный', 23000),
(3, 3, 'Сургут, Быстринская 22', '65', 'продажа', 'первичный', 550000),
(4, 4, 'Сургут, Островского 4', '40', 'покупка', 'вторичный', 250000),
(5, 5, 'пос. Белый Яр, Авиаторов 3', '100', 'аренда', 'вторичный', 55000),
(6, 6, 'пос. Солнечный,  Космонавтов 35', '250', 'аренда', 'первичный', 45230),
(7, 7, 'Сургут, Гагарина 10', '160', 'продажа', 'первичный', 2000000);

-- --------------------------------------------------------

--
-- Структура таблицы `house`
--

CREATE TABLE `house` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `floor` varchar(20) NOT NULL COMMENT 'колво этажей',
  `quantity_room` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'колво комнат',
  `square` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'площадь',
  `typeofprop` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'тип недвижимости',
  `typeofdeal` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'тип сделки',
  `price` varchar(20) NOT NULL COMMENT 'цена',
  `adress` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'Адрес'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `house`
--

INSERT INTO `house` (`id`, `user_id`, `floor`, `quantity_room`, `square`, `typeofprop`, `typeofdeal`, `price`, `adress`) VALUES
(1, 1, '2', '6', '130', 'продажа', 'вторичный', '2650000', 'днт Летние юрты,  5я д20'),
(2, 2, '1', '2', '80', 'вторичный', 'покупка', '1500000', 'дп. Победит-1, правая 12'),
(3, 3, '2', '4', '240', 'первичный', 'аренда', '65000', 'днт, Мостовик'),
(4, 4, '1', '1', '60', 'вторичный', 'продажа', '450000', 'Сургут, Киртбая 10'),
(5, 5, '2', '6', '320', 'вторичный', 'аренда', '45000', 'пос. Снежный, ул Еловая 1'),
(6, 6, '1', '1', '30', 'вторичный', 'аренда', '25000', 'ск Победит-1,  5я левая 18');

-- --------------------------------------------------------

--
-- Структура таблицы `landplot`
--

CREATE TABLE `landplot` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `adress` varchar(50) NOT NULL COMMENT 'адрес',
  `square` int NOT NULL COMMENT 'площадь',
  `typeofdeal` varchar(20) NOT NULL,
  `typeofprop` varchar(20) NOT NULL,
  `price` int NOT NULL COMMENT 'цена'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `landplot`
--

INSERT INTO `landplot` (`id`, `user_id`, `adress`, `square`, `typeofdeal`, `typeofprop`, `price`) VALUES
(1, 1, 'пос.Барсово,  7я правая 5', 6, 'продажа', 'вторичный', 1200000),
(2, 2, 'дп. Победит-1, левая 10', 3, 'покупка', 'первичный', 650000),
(3, 3, 'днт Летние юрты,  1я д10', 12, 'продажа', 'вторичный', 850000),
(4, 4, 'снт Черемушки, светлая 1', 5, 'покупка', 'вторичный', 980000),
(5, 5, 'днт Мостовик, 1я 25', 5, 'продажа', 'первичный', 740000),
(6, 6, 'Сургут, Мелик-Карамова 1', 15, 'покупка', 'первичный', 1850000),
(7, 3, 'пос.Барсово,  7я левая 5', 14, 'продажа', 'вторичный', 1230500);

-- --------------------------------------------------------

--
-- Структура таблицы `typeofdeal`
--

CREATE TABLE `typeofdeal` (
  `id` int NOT NULL,
  `rent` varchar(20) NOT NULL COMMENT 'аренда',
  `purchase` varchar(20) NOT NULL COMMENT 'покупка',
  `sale` varchar(20) NOT NULL COMMENT 'продажа'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `typeofprop`
--

CREATE TABLE `typeofprop` (
  `id` int NOT NULL,
  `primary` varchar(20) NOT NULL COMMENT 'первичное',
  `secondary` varchar(20) NOT NULL COMMENT 'вторичное'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `login` varchar(20) NOT NULL COMMENT 'логин',
  `password` varchar(20) NOT NULL COMMENT 'пароль'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `password`) VALUES
(1, 'user1', 'user1'),
(2, 'user2', 'user3'),
(3, 'user3', 'user3'),
(4, 'user4', 'user4'),
(5, 'user5', 'user5'),
(6, 'user6', 'user6'),
(7, 'user7', 'user7');

-- --------------------------------------------------------

--
-- Структура таблицы `viewofprop`
--

CREATE TABLE `viewofprop` (
  `id` int NOT NULL,
  `house` varchar(20) NOT NULL COMMENT 'загородный дом',
  `apartament` varchar(20) NOT NULL COMMENT 'квартира',
  `comercialprop` varchar(20) NOT NULL COMMENT 'коммерческая недвижимость',
  `garage` int NOT NULL COMMENT 'гараж',
  `landplot` int NOT NULL COMMENT 'земельный участок'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `apartament`
--
ALTER TABLE `apartament`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `commercialprop`
--
ALTER TABLE `commercialprop`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `garage`
--
ALTER TABLE `garage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `house`
--
ALTER TABLE `house`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `landplot`
--
ALTER TABLE `landplot`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `typeofdeal`
--
ALTER TABLE `typeofdeal`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `typeofprop`
--
ALTER TABLE `typeofprop`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `viewofprop`
--
ALTER TABLE `viewofprop`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `apartament`
--
ALTER TABLE `apartament`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `commercialprop`
--
ALTER TABLE `commercialprop`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `garage`
--
ALTER TABLE `garage`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `house`
--
ALTER TABLE `house`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `landplot`
--
ALTER TABLE `landplot`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `typeofdeal`
--
ALTER TABLE `typeofdeal`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `typeofprop`
--
ALTER TABLE `typeofprop`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `viewofprop`
--
ALTER TABLE `viewofprop`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `apartament`
--
ALTER TABLE `apartament`
  ADD CONSTRAINT `apartament_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `commercialprop`
--
ALTER TABLE `commercialprop`
  ADD CONSTRAINT `commercialprop_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `garage`
--
ALTER TABLE `garage`
  ADD CONSTRAINT `garage_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `house`
--
ALTER TABLE `house`
  ADD CONSTRAINT `house_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `landplot`
--
ALTER TABLE `landplot`
  ADD CONSTRAINT `landplot_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
